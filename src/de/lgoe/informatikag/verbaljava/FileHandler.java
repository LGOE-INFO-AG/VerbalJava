package de.lgoe.informatikag.verbaljava;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class FileHandler {
		ArrayList <Student> studentList = new ArrayList<Student>();
		File f;
		
		public FileHandler () {
			File file = new File(".");
			File[] filesa = file.listFiles();
			List <File> files = Arrays.asList(filesa);
				for(File fall : files) {
					if(fall.getName().endsWith(".vbd")){
						try {
							f = fall;
							FileReader ff = new FileReader(fall);
							BufferedReader bf = new BufferedReader(ff);
							String s;
							while ((s = bf.readLine()) != null){
								if(s.equals(""))
									continue;
								if(s.startsWith("#")) {
									Student st = new Student(s);
									st.setText(bf.readLine());
									studentList.add(st);
								}
							}
							bf.close();
							ff.close();
						} catch (IOException e) {
							e.printStackTrace();
						}						
					}
				}


		}
		
		public List<Student> getStudents() {
			return studentList;
		}
		
		public void save() {
			try {
				FileWriter fw = new FileWriter(f);
				BufferedWriter bw = new BufferedWriter(fw);
				for(Student all : studentList) {
					bw.write(all.getFileText());
					bw.newLine();
					String intext = all.getText();
					String goodtext = intext.replaceAll("[^a-zA-zÄÖÜäöüß,. ]", "");
					all.setText(goodtext);
					bw.write(goodtext);
					bw.newLine();
				}
				bw.close();
				fw.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
}
