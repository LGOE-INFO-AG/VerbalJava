package de.lgoe.informatikag.verbaljava;

public class Student {
		
		String clazz;
		String lastName;
		String firstName;
		String birthDate;
		String text;
		
		public Student (String s){
			s = s.replaceFirst("#", "");
			String[] sa = s.split("#");
			clazz = sa[0];
			s = s.replaceFirst(clazz + "#", "");
			sa = s.split("#");
			lastName = sa[0];
			s = s.replaceFirst(lastName + "#", "");
			sa = s.split("#");
			firstName = sa[0];
			s = s.replaceFirst(firstName + "#", "");
			sa = s.split("#");
			birthDate = sa[0];
		
		}
		
		public String getFileText() {
			//#05a#Bergfrau#Mara#07.04.07
			String text = "#"+clazz+"#"+lastName+"#"+firstName+"#"+birthDate;
			return text;
		}
		
		public String getText() {
			return text;
		}
		
		public void setText(String text) {
			this.text = text.replace("\n", " ");
		}

		public String getClazz() {
			return clazz;
		}

		public void setClazz(String clazz) {
			this.clazz = clazz;
		}

		public String getLastName() {
			return lastName;
		}

		public void setLastName(String lastName) {
			this.lastName = lastName;
		}

		public String getFirstName() {
			return firstName;
		}

		public void setFirstName(String firstName) {
			this.firstName = firstName;
		}

		public String getBirthDate() {
			return birthDate;
		}

		public void setBirthDate(String birthDate) {
			this.birthDate = birthDate;
		}
}
