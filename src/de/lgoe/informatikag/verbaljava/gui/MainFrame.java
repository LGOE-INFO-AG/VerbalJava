package de.lgoe.informatikag.verbaljava.gui;

import javax.swing.*;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import de.lgoe.informatikag.verbaljava.Student;
import de.lgoe.informatikag.verbaljava.VerbalJava;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;

/**
 * The class {@code MainFrame} is a form of {@code JFrame}
 * that creates a new frame
 *
 * @author Jannik Emmerich
 * @author Benedikt Wolf
 */
public class MainFrame extends JFrame implements ActionListener, ListSelectionListener {

	//Initialize object references
	JButton saveButton;
	JButton helpButton;
	JTextArea textField;
	JList list;
	
	//Initialize variables
	int selectedStudent = -1;
	
	//Put text for help dialog here:
	String helpString = "W\u00e4hlen Sie einen Namen aus der Liste (links) und geben Sie den zugeh\u00f6rigen Text (rechts) ein. \n Zum Speichern dr\u00fccken Sie Speichern oder schlie\u00dfen Sie das Programm.";
	
	
    /**
     * Constructs a new instance of {@code MainFrame}
     */
    @SuppressWarnings({ "unchecked", "rawtypes" })
	public MainFrame() {

        //Set the title to 'VerbalJava'
        this.setTitle("VerbalJava");

        //Set the default close operation to 'EXIT_ON_CLOSE'
        //TODO change EXIT_ON_CLOSE to custom function to save and exit / add close "Do you want to quit without saving" dialog
        this.setDefaultCloseOperation(EXIT_ON_CLOSE);

        //Show the frame
        this.setVisible(true);

        //Set up variables with the width of the frame and the
        //height of the frame to make sure, the frame has the same
        //proportional size every start
        Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
        double width_screen = dim.getWidth();
        double width_frame = width_screen * 4 / 6;
        double height_frame = width_frame * 1 / 2;
        this.setSize((int) width_frame,(int) height_frame);

        //Set location to the center of the screen
        this.setLocation(dim.width/2-this.getSize().width/2, dim.height/2-this.getSize().height/2);

       List<String> studentNames = new ArrayList<>();
        List<Student> students = VerbalJava.getFileHandler().getStudents();
        for(Student s : students) {
        	String name = s.getFirstName();
        	String lastName = s.getLastName();
        	studentNames.add(lastName + ", " + name);
        }	
        
        //TODO Make comments
        getContentPane().setLayout(new BorderLayout());
        
        //Text field with explanation how to use the program
        helpButton = new JButton("Hilfe");
        helpButton.setPreferredSize(new Dimension(100,30));
        helpButton.addActionListener(this);
        getContentPane().add(helpButton,  BorderLayout.PAGE_START);
        
        list = new JList(studentNames.toArray());
        //list.setPreferredSize(new Dimension((int) (width_frame / 5 - 10),(int) height_frame - 75));
        list.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        list.addListSelectionListener(this);
        JScrollPane scrollPaneList = new JScrollPane(list);
        scrollPaneList.setPreferredSize(new Dimension((int) (width_frame / 5 + 10 ),(int) height_frame - 37));
        getContentPane().add(scrollPaneList, BorderLayout.LINE_START);

        //TODO Find a solution for the problem with the text area (Make size proportional to the screen size)
        textField = new JTextArea((int) height_frame / 25, (int) (width_frame - 20) / 14);
        textField.setLineWrap(true);
        textField.setWrapStyleWord(true);
        textField.setEditable(false);
        JScrollPane scrollPaneText = new JScrollPane(textField);
        getContentPane().add(scrollPaneText, BorderLayout.LINE_END);

        //TODO Find a solution for the problem with the save button (the position of the save button is no good)
        saveButton = new JButton("Speichern");
        saveButton.setPreferredSize(new Dimension(100, 30));
        saveButton.addActionListener(this);
        getContentPane().add(saveButton, BorderLayout.PAGE_END);
        
                
        //Make the frame not resizable, this has to be done after the rest or the frame is not properly constructed
        this.setResizable(false);
    }
    
    public void valueChanged (ListSelectionEvent evt) {
    	
    	textField.setEditable(true);
    	
    	if(selectedStudent != -1){
        	String text = textField.getText();
        	Student previousStudent = VerbalJava.getFileHandler().getStudents().get(selectedStudent);
        	previousStudent.setText(text);
    	}
    	
    	//Get selected student
    	String currentStudent = list.getSelectedValue().toString();
    	String[] s = currentStudent.split(", ");
    	String lastName = s[0];
    	String firstName = s[1];
    	
    	for(Student all : VerbalJava.getFileHandler().getStudents()){
    		if(all.getFirstName().equals(firstName) && all.getLastName().equals(lastName)) {
    			textField.setText(all.getText());
    		}
    	}
    	
    	if(selectedStudent != -1) {
    		VerbalJava.getFileHandler().save();
    	}
    	
    	selectedStudent = list.getSelectedIndex();
    	
    }
    
    public void actionPerformed (ActionEvent evt) {
    	if(evt.getSource() == saveButton ) {
    		if(selectedStudent != -1){
            	String text = textField.getText();
            	Student previousStudent = VerbalJava.getFileHandler().getStudents().get(selectedStudent);
            	previousStudent.setText(text);
        	}
    		VerbalJava.getFileHandler().save();
    	}else if(evt.getSource()==helpButton) {
    		//Open help dialog
    		JOptionPane.showMessageDialog(rootPane, helpString, "Hilfe", JOptionPane.INFORMATION_MESSAGE);			
    	}
    }
}
