package de.lgoe.informatikag.verbaljava;

import de.lgoe.informatikag.verbaljava.gui.MainFrame;

import java.awt.*;
import java.lang.reflect.InvocationTargetException;

/**
 * The class {@code VerbalJava} is the main class of the
 * project VerbalJava that starts the frame {@code MainFrame}
 * and the main program
 *
 * @author Jannik Emmerich
 * @author Benedikt Wolf
 */
public class VerbalJava {

	private static FileHandler fileHandler;
	
    /**
     * Creates a new instance of {@link MainFrame}
     *
     * @param args The arguments give by the command line
     * @throws InvocationTargetException if there is a problem with the Runnable
     * @throws InterruptedException if there is a problem with the Runnable
     */
    public static void main(String[] args) throws InvocationTargetException, InterruptedException {
    	fileHandler = new FileHandler();    	
        EventQueue.invokeAndWait(new Runnable() {
            @Override
            public void run() {
                new MainFrame();
            }
        });
    }
    
    public static FileHandler getFileHandler(){
    	return fileHandler;
    }
}
